package com.thechalakas.jay.listviewdemo1;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //final ListView listview = (ListView) findViewById(R.id.listview);

        //get the listview object

        ListView listView = (ListView) findViewById(R.id.listview);

        //data source
        String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux",
                "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux", "OS/2",
                "Android", "iPhone", "WindowsMobile" };

        //local list collection of operating systems

        final ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < values.length; ++i)
        {
            //add every operating system name to the list
            //we are converting the string array into a list collection
            //our list view works with list collections only. it wont work with string array
            list.add(values[i]);
        }

        //log the collection right away

        for(int i=0;i<values.length;i++)
        {
            Log.d("listview1","valueo of is is " + i + " store value is " + values[i]);
        }

        //alright list collection works

        //lets use the adapter we have defined in another class file

        //note the simple_list_item_1 layout. thats provided by default by android
        //in a further scenario, we will use our own layouts.
        //step 1, connect the adapter to the list of data we have


        //StableArrayAdapter adapter = new StableArrayAdapter(this,android.R.layout.simple_list_item_1,list);

        //step 2, connect the adapter to the UI element we have.
        //listView.setAdapter(adapter);

        //alright, lets use the new adapter

        //step 1 create the adapter and connect it to the data source
        //unlike the previous effort, this adapter takes a string array
        //so instead of using the list we are using the string array of values directly

        MySimpleArrayAdapter adapter2 = new MySimpleArrayAdapter(this,values);

        //step 2 set the adapter to the list itself.
        listView.setAdapter(adapter2);

    }
}

