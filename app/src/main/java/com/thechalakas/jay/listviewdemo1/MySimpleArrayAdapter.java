package com.thechalakas.jay.listviewdemo1;
/*
 * Created by jay on 02/11/17. 7:24 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//this is the adapter for the custom view and custom layout

public class MySimpleArrayAdapter extends ArrayAdapter<String>
{
    private final Context context;
    private final String[] values;

    //we get the app context
    public MySimpleArrayAdapter(Context context, String[] values)
    {
        super(context, R.layout.rowlayout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        //get a inflater which will 'inflate the view
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //grab the row that needs to be set with values
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);

        //now, use the row to select the text view that needs to be set with some values
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        //now that you have a handle on the text view object, go ahead and set whatever value you want.
        //in this case the name of the operating system.
        textView.setText(values[position]);

        //yeah, ignoring the image stuff
        //ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        //the referenced tutorial talks about some icon stuff
        //its not important to me so I just dont want to use that.

        /*
        // Change the icon for Windows and iPhone
        String s = values[position];
        if (s.startsWith("Windows7") || s.startsWith("iPhone")
                || s.startsWith("Solaris"))
        {
            imageView.setImageResource(R.drawable.no);
        }
        else
        {
            imageView.setImageResource(R.drawable.ok);
        }
        */

        return rowView;
    }
}
