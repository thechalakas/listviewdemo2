package com.thechalakas.jay.listviewdemo1;
/*
 * Created by jay on 02/11/17. 4:27 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.HashMap;
import java.util.List;

//this is the basic adapter for the list
//remember that an adapter is like an interface between the code file and the UI file.
//you know, it adapts the data to the view

public class StableArrayAdapter extends ArrayAdapter<String>
{

    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

    public StableArrayAdapter(Context context, int textViewResourceId,
                              List<String> objects)
    {
        super(context, textViewResourceId, objects);
        for (int i = 0; i < objects.size(); ++i)
        {
            mIdMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position)
    {
        String item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds()
    {
        return true;
    }

}
